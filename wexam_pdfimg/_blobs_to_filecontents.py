# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


from typing import Iterable

import exif
import img2pdf

from ._jpegpage import JpegPage

A4_MM = (2 ** (-1 / 4 - 2) * 1000, 2 ** (1 / 4 - 2) * 1000)
LAYOUT_FUN_A4 = img2pdf.get_layout_fun([img2pdf.mm_to_pt(x) for x in A4_MM])

EXIF_ROTATIONS = {0: 1, 90: 6, 180: 3, 270: 8}


class OrientationError(Exception):
    pass


def blob_to_jpgfile(jpegpage: JpegPage) -> bytes:
    if jpegpage.orientation not in EXIF_ROTATIONS:
        raise OrientationError

    img = exif.Image(jpegpage.content)
    img.set("orientation", EXIF_ROTATIONS[jpegpage.orientation])
    return img.get_file()


def blobs_to_pdffile(jpegpages: Iterable[JpegPage]) -> bytes:
    jpegpages = list(jpegpages)
    for jpegpage in jpegpages:
        if jpegpage.orientation not in EXIF_ROTATIONS:
            raise OrientationError

    imgs = []
    for jpegpage in jpegpages:
        if jpegpage.content[:4] == b"\x89PNG":
            imgs.append(jpegpage.content)
        else:
            img = exif.Image(jpegpage.content)
            img.set("orientation", EXIF_ROTATIONS[jpegpage.orientation])
            imgs.append(img.get_file())

    out = img2pdf.convert(imgs, layout_fun=LAYOUT_FUN_A4)
    return out
