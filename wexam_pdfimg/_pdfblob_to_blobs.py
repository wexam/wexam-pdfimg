# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import io
import re
import subprocess
import collections
from typing import Tuple, Iterator

from PIL import Image

from ._jpegpage import JpegPage


class PdfReadError(Exception):
    pass


class PdfNumberOfPagesError(Exception):
    pass


def _split_A3_pages_in_two_A4(jpegpage: JpegPage) -> Tuple[JpegPage]:
    im = Image.open(io.BytesIO(jpegpage.content))
    width, height = im.size
    if width < height:
        im = im.transpose(Image.ROTATE_90)
    out1 = io.BytesIO()
    im.crop((0, 0, width // 2, height)).save(
        out1, format="jpeg", optimize=True, quality=75
    )
    out2 = io.BytesIO()
    im.crop((width // 2 + 1, 0, width, height)).save(
        out2, format="jpeg", optimize=True, quality=75
    )
    return (JpegPage(content=out1.getvalue()), JpegPage(content=out2.getvalue()))


class RawPdf:
    _content: bytes
    _infos: dict
    _pages: int
    _pages_jpg: collections.defaultdict

    def __init__(self, content: bytes, quality=75):
        self._content = content
        self._pages_jpg = collections.defaultdict(lambda: None)
        self._quality = quality

    @property
    def infos(self) -> dict:
        if not hasattr(self, "_infos"):
            self._retreive_infos()
        return self._infos

    @property
    def pages(self) -> int:
        if not hasattr(self, "_pages"):
            self._pages = int(self.infos["Pages"])
        return self._pages

    def __len__(self) -> int:
        return self.pages

    def _retreive_infos(self):
        child = subprocess.Popen(
            ("pdfinfo", "-"),
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        out, _ = child.communicate(self._content)
        if child.returncode != 0:
            raise PdfReadError
        self._infos = {
            ls[0]: re.sub("^ +", "", ":".join(ls[1:]))
            for ls in (l.split(":") for l in out.decode().splitlines() if l)
        }

    def _get_jpg_page(self, pageno: int) -> JpegPage:
        if self._pages_jpg[pageno] is None:
            child = subprocess.Popen(
                (
                    "pdftoppm",
                    "-r",
                    "150",
                    "-jpeg",
                    "-jpegopt",
                    f"quality={self._quality},optimize=y",
                    "-",
                    "-f",
                    f"{pageno:d}",
                    "-singlefile",
                ),
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            )
            out, _ = child.communicate(self._content)
            if child.returncode != 0:
                raise PdfReadError
            self._pages_jpg[pageno] = JpegPage(out)
        return self._pages_jpg[pageno]

    def __getitem__(self, key: int) -> JpegPage:
        if isinstance(key, slice):
            return tuple(self[i] for i in range(self.pages)[key])
        if not isinstance(key, int):
            raise TypeError
        if not 0 <= key < self.pages:
            raise IndexError
        return self._get_jpg_page(key + 1)

    def __iter__(self) -> Iterator[JpegPage]:
        return (self[key] for key in range(self.pages))


class A4_1S:
    _raw: RawPdf
    _groups: int
    _ppg = 1

    def __init__(self, content: bytes):
        self._raw = RawPdf(content)
        self._groups = self._raw.pages

    @property
    def groups(self) -> int:
        return self._groups

    @classmethod
    @property
    def ppg(cls) -> int:
        return cls._ppg

    def __len__(self) -> int:
        return self.groups

    def __getitem__(self, key: int) -> Tuple[JpegPage]:
        if isinstance(key, slice):
            return tuple(self[i] for i in range(self.groups)[key])
        if not isinstance(key, int):
            raise TypeError
        if not 0 <= key < self.groups:
            raise IndexError
        return (self._raw[key],)

    def __iter__(self) -> Iterator[Tuple[JpegPage]]:
        return (self[key] for key in range(self.groups))


class A4:
    _raw: RawPdf
    _groups: int
    _ppg: int

    def __init__(self, content: bytes, ppg: int):
        self._raw = RawPdf(content)
        self._ppg = ppg
        if self._raw.pages % self._ppg != 0:
            raise PdfNumberOfPagesError
        self._groups = self._raw.pages // self._ppg

    @property
    def groups(self) -> int:
        return self._groups

    @property
    def ppg(self) -> int:
        return self._ppg

    def __len__(self) -> int:
        return self.groups

    def __getitem__(self, key: int) -> Tuple[JpegPage]:
        if isinstance(key, slice):
            return tuple(self[i] for i in range(self.groups)[key])
        if not isinstance(key, int):
            raise TypeError
        if not 0 <= key < self.groups:
            raise IndexError
        return self._raw[(self._ppg * key) : (self._ppg * key + self._ppg)]

    def __iter__(self) -> Iterator[Tuple[JpegPage]]:
        return (self[key] for key in range(self.groups))


class A3_double:
    _raw: RawPdf
    _groups: int
    _ppg = 4

    def __init__(self, content):
        self._raw = RawPdf(content, quality=100)
        if self._raw.pages % 2 != 0:
            raise PdfNumberOfPagesError
        self._groups = self._raw.pages // 2

    @property
    def groups(self) -> int:
        return self._groups

    @classmethod
    @property
    def ppg(cls) -> int:
        return cls._ppg

    def __len__(self) -> int:
        return self.groups

    def __getitem__(self, key: int) -> Tuple[JpegPage]:
        if isinstance(key, slice):
            return tuple(self[i] for i in range(self.groups)[key])
        if not isinstance(key, int):
            raise TypeError
        if not 0 <= key < self.groups:
            raise IndexError
        big_page1, big_page2 = self._raw[(2 * key) : (2 * key + 2)]
        page1, page2 = _split_A3_pages_in_two_A4(big_page1)
        page3, page4 = _split_A3_pages_in_two_A4(big_page2)
        return (page2, page3, page4, page1)

    def __iter__(self) -> Iterator[Tuple[JpegPage]]:
        return (self[key] for key in range(self.groups))
